<?php
$reg = 0;
$jf = new JfVenta($_SESSION['id']);
$jf->consultar();
include 'presentacion/jefe/menuJF.php';

?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-primary text-white">Bienvenido Administrador</div>
				<div class="card-body">
					<p>Usuario: <?php echo $jf -> getNombre() . " " . $jf -> getApellido() ?></p>
					<p>Correo: <?php echo $jf -> getCorreo(); ?></p>
					<p>Hoy es: <?php echo date("d-M-Y"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>