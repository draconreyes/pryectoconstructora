<?php
$jfe = new JfVenta();
$ped = $jfe->consultarPedidos($_REQUEST['valor']);
?>

<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th scope="col">Id</th>
			<th scope="col">Obrero</th>
			<th scope="col">Tipo</th>
			<th scope="col">Servicio</th>
		</tr>
	</thead>
	<tbody>
<?php
foreach ($ped as $p) {
        // Esta capa correspondiente a la fila del paciente a actualizar permitira agregar subcapas de estado y el candado a cambiar
        echo "<tr id='pac" . $p->getId() . "'>";

        echo "<td>" . $p->getId() . "</td>";

        echo "<td>" . $p->getNombre()." ". $p->getApellido()."</td>";

        echo "<td>" . $p->getCorreo() . "</td>";

        echo "<td>" ."<a href='index.php?pid=".base64_encode("presentacion/jefe/modalPedido.php")."&idc=".$p->getId()."' data-toggle='modal' data-target='#modalPedido' >
						           <span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left'
						           data-original-title='Ver informacion' ></span> </a>
                      <a class='fas fa-share' href='index.php?pid=" . base64_encode("presentacion/jefe/enviarP.php") . "&idc=" . $p->getId() . "' data-toggle='tooltip' data-placement='left' title='Enviar Material'> </a>". 
        
                        "</td>";
        
        echo "</tr>";
    }
    echo "<tr><td colspan='9'>" . count($ped) . " registros encontrados</td></tr>"?>
						</tbody>
</table>




<script type="text/javascript">
$(document).ready(function(){	
	<?php foreach ($ped as $p) { ?>
		$("#hab<?php echo $p[1];?>").click(function(){
			<?php echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/paciente/editarEstadoPacienteAjax.php") . "&idPaciente=" . $p -> getId() . "&estado=" . $p -> getEstado() . "\";"; ?>
			// Esto esconde el Tooltip del candado previamente seleccionado
			$("#hab<?php echo $p -> getId();?>").tooltip('hide');
			// Esto carga toda la capa de la fila de la tabla del paciente a actualizar vease arriba que la etiqueta <tr> contiene el id pac#
			$("#pac<?php echo $p -> getId();?>").load(ruta);
		});
		<?php } ?>
	});
	</script>

