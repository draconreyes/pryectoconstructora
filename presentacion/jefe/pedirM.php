<?php
$jf = new JfVenta($_SESSION['id']);
$jf->consultar();
$idma= $_GET["idm"];
if (isset($_POST["pedir"])) {

    $cantidad = $_POST["cantidad"];
    $material = new Material($idma, "",$cantidad, "");
    $material->pedirMa();
}
include 'presentacion/jefe/menuJF.php';
?>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header bg-primary text-white">Pedir Material</div>
				<div class="card-body">
						<?php
                        if (isset($_POST["pedir"])) {
                         ?>
						<div class="alert alert-success" role="alert">Pedido exitoso.</div>						
						<?php } ?>
						<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/jefe/pedirM.php")."&idm=".$_GET["idm"] ?>
						method="post">

						<div class="form-group">
							<input type="number" name="cantidad" class="form-control"  min="1" max="30"
								placeholder="Cantidad" required=""
								value="">
						</div>
						
						<button type="submit" name="pedir" class="btn btn-primary">Pedir</button>
					</form>
				</div>
			</div>
		</div>

	</div>

</div>