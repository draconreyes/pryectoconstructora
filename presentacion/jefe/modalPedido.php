<?php
$reg =0;
$idc= $_GET['idc'];
$ped = new pedido('','',$idc,'');
$peds= $ped->materiales();

?>
<div class="modal-header">
    <h5 class="modal-title">Materiales Solicitados</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table table-striped table-hover">
       <thead>
		<tr>
			<th scope="col">Material</th>
			<th scope="col">Cantidad en Inventario</th>
			<th scope="col">Cantidad solicitada</th>
		</tr>
	</thead>
	<tbody>
<?php
foreach ($peds as $p) {
        // Esta capa correspondiente a la fila del paciente a actualizar permitira agregar subcapas de estado y el candado a cambiar
        echo "<tr id='pac'>";
        echo "<td> " . $p->getIdpedido(). " </td>";

        echo "<td>" . $p->getCantidad()."</td>";
        
        if($p->getInventario_idinventario()>$p->getCantidad()){
        echo "<td> <font color='red'>" . $p->getInventario_idinventario() . "</font> </td>";        
        }else{
        echo "<td> <font color='green'>" . $p->getInventario_idinventario() . "</font> </td>";
        }
 
        echo "</tr>";
    }
   // echo "<tr><td colspan='9'>" . count($ped) . " registros encontrados</td></tr>"?>
						</tbody>
    </table>
</div>
