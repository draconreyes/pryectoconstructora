<?php
$reg=0;
$jf = new JfVenta($_SESSION['id']);
$jf->consultar();
$idc= $_GET['idc'];
if(isset($_POST['enviar'])){
    $env = new pedido('', '',$_POST['idc'], '');
    echo $_POST['idc'];
    $pe = $env->materiales();
    foreach ($pe as $p){
     $jf->enviar($p->getIdpedido(),$p->getInventario_idinventario());
    }
    
    $jf->cambiar($_POST['idc']);
    header("Location: index.php?pid=". base64_encode("presentacion/jefe/sesionJefe.php"));
}else{
    
    $ped = new pedido('', '',$_GET['idc'], '');
    $peds = $ped->materiales();
}

include 'presentacion/jefe/menuJF.php';
?>

<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card" style="margin-top: 100px;">
				<div class="card-header bg-primary text-white text-center py-4">Enviar
					material</div>
				<div class="card-body">
					<form class="border border-light p-5"
						action="index.php?pid=<?php echo base64_encode("presentacion/jefe/enviarP.php");?>"
						method="post" enctype="multipart/form-data">
						<input type="hidden" name="idc" required id="idc" class="form-control mb-4"
							placeholder="Nombre" value="<?php echo $idc;?>"> 
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th scope="col">Material</th>
									<th scope="col">Cantidad en Inventario</th>
									<th scope="col">Cantidad solicitada</th>
								</tr>
							</thead>
							<tbody>
<?php
foreach ($peds as $p) {
    echo "<tr id='pac'>";
    echo "<td> " . $p->getIdpedido() . " </td>";

    echo "<td>" . $p->getCantidad() . "</td>";

    if ($p->getInventario_idinventario() > $p->getCantidad()) {
        echo "<td> <font color='red'>" . $p->getInventario_idinventario() . "</font> </td>";
        $reg=1;
    } else {
        echo "<td> <font color='green'>" . $p->getInventario_idinventario() . "</font> </td>";
    }

    echo "</tr>";
}
 ?>
						</tbody>
						</table>
						<?php if($reg==0){  ?>
					<button type="input" class="btn btn-primary" name="enviar">ENVIAR
							</button>
							<?php }else {
							echo "DEBE TENER TODOS LOS MATERIALES DISPONIBLES PARA EL ENVIO";
							}?>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>

