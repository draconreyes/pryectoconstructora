<?php
$error=-1;
$nombre = "";
$precio = "";
$cantidad="";
$jf = new JfVenta( $_SESSION ['id'] );
if (isset ( $_POST ['registro'] )) {
    $nombre = $_POST ['nombre'];
    $precio = $_POST ['precio'];
    $cantidad= $_POST['cantidad'];
    $material = new Material( "", $nombre, $cantidad, $precio);
    if ($material->existeM()) {
        $error = 1;
    } else {
        $material->registrar();
        $error=0;
    }
}
include 'presentacion/jefe/menuJF.php';
?>

<div class="container">
	<div class="row">

</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card" style="margin-top: 100px;">
				<div class="card-header bg-primary text-white text-center py-4">Ingresar Material</div>
				<div class="card-body">
					<form class="border border-light p-5"
						action="index.php?pid=<?php echo base64_encode("presentacion/jefe/crearM.php");?>"
						method="post">
						<?php if($error == 1){?>
							<div class="alert alert-danger" role="alert">Material ya existente.</div>
						<?php }else if ($error == 0){ ?>
						<div class="alert alert-success" role="alert">Material Creado</div>
						<?php }?>

						<input type="text" name="nombre" required id="textInput" class="form-control mb-4"
							placeholder="Nombre" value="<?php echo $nombre;?>"> 
						
						<input type="number" name="cantidad" required id="textInput" class="form-control mb-4" min="1" max="20"
							placeholder="Cantidad" value="<?php echo $cantidad;?>">		
							
						<input type="number" name="precio" required id="textInput" class="form-control mb-4"
							placeholder="Precio" value="<?php echo $precio;?>">

						<button type="input" class="btn btn-primary" name="registro"> Crear Material</button>
						


					</form>
				</div>
			</div>
		</div>

	</div>
</div>