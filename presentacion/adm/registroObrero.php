<?php
$error=0;
$nombre = "";
$apellido = "";
$correo = "";
$clave = "";
$telefono="";
$errors = array();
$administrador = new Administrador ( $_SESSION ['id'] );
if (isset ( $_POST ['registro'] )) {
	$nombre = $_POST ['nombre'];
	$apellido = $_POST ['apellido'];
	$correo = $_POST ['correo'];
	$clave = $_POST ['clave'];
    $telefono=$_POST['telefono'];
	if (isset($_FILES['foto'])) {
	    $obrero = new Obrero( "", $nombre, $apellido, $correo, $clave, $telefono,"");
	    $errors = array();
	    $extensions = array("jpeg", "jpg", "png");
	    $ext_archivo = explode(".", $_FILES['foto']['name']);
	    $ext_archivo = end($ext_archivo);
	    if (in_array($ext_archivo, $extensions) === false) {
	        $errors[] = "Extension no permitada, escoja un archivo con extension JPEG, PNG o JPG.";
	    }
	    if ($_FILES['foto']['size'] > 2097152) {
	        $errors[] = "El archivo no debe pesar mas de 2MB";
	    }
	    if (empty($errors)) {
	        $hora = round(microtime(true) * 1000);
	        $nombreFoto = $hora . "." . $ext_archivo;
	        move_uploaded_file($_FILES['foto']['tmp_name'], "img/" . $nombreFoto);
	        if ($obrero->existeCorreo()) {
	            $error = 1;
	        } else {
	            $obrero->setFoto($nombreFoto);
	            $obrero->registrar();
	            header("Location: index.php?pid=". base64_encode("presentacion/sesionAdministrador.php")."&obr=true&nombre= ". $obrero->getNombre()."&apellido= ". $obrero->getApellido() );
	        }
	    }
	}
	
}
?>
<div class="container">
	<div class="row">
<?php
include 'presentacion/menuAdministrador.php';
?>
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card" style="margin-top: 100px;">
				<div class="card-header bg-primary text-white text-center py-4">Registro Obrero</div>
				<div class="card-body">
					<form class="border border-light p-5"
						action="index.php?pid=<?php echo base64_encode("presentacion/adm/registroObrero.php");?>"
						method="post" enctype="multipart/form-data">
	
						<?php if($error == 1){?>
							<div class="alert alert-danger" role="alert">Correo ya
							registrado, ingrese otro.</div>
						<?php }?>
						<?php
						if (!empty($errors)) {?>
						    <div class="alert alert-danger" role="alert"><?php echo $errors[0] ?></div>
					  <?php }?>
				

						<input type="text" name="nombre" required id="textInput" class="form-control mb-4"
							placeholder="Nombre" value="<?php echo $nombre;?>"> 
							
						<input type="text" name="apellido" required id="textInput" class="form-control mb-4"
							placeholder="Apellido" value="<?php echo $apellido;?>">

						<input type="text" name="telefono" required id="textInput" class="form-control mb-4"
							placeholder="Telefono" value="<?php echo $telefono;?>"> 

						<input type="email" name="correo" required id="emailInput" class="form-control mb-4"
							placeholder="Correo"> 
						<input type="text" id="passwdInput" name="clave" required
							class="form-control mb-4" placeholder="Clave">
							<div class="form-group">
							<input type="file" name="foto" class="form-control" placeholder="Foto" required="required">
							</div>
					 		<small
							id="defaultRegisterFormPhoneHelpBlock"
							class="form-text text-muted mb-4">Debe tener mayusculas,
							minusculas.
							</small> 
						<button type="input" class="btn btn-primary" name="registro">Registrar
							usuario</button>
						<a
							href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php")?>"><button
							type="button" class="btn btn-secondary">Volver</button></a>


					</form>
				</div>
			</div>
		</div>

	</div>
</div>