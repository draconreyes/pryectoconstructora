<?php
$error=0;
$nombre = "";
$apellido = "";
$correo = "";
$clave = "";
$telefono="";
$administrador = new Administrador ( $_SESSION ['id'] );
if (isset ( $_POST ['registro'] )) {
	$nombre = $_POST ['nombre'];
	$apellido = $_POST ['apellido'];
	$correo = $_POST ['correo'];
	$clave = $_POST ['clave'];
    $telefono=$_POST['telefono'];
	$usuario = new Usuario( "", $nombre, $apellido, $correo, $clave, $telefono);
	if ($usuario->existeCorreo ()) {
		$error = 1;
	} else {
	    $usuario->registrar();
		header("Location: index.php?pid=". base64_encode("presentacion/sesionAdministrador.php")."&usu=true&nombre= ". $usuario->getNombre()."&apellido= ". $usuario->getApellido() );
	}
}
?>
<div class="container">
	<div class="row">
<?php
include 'presentacion/menuAdministrador.php';
?>
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card" style="margin-top: 100px;">
				<div class="card-header bg-primary text-white text-center py-4">Registro Usuario</div>
				<div class="card-body">
					<form class="border border-light p-5"
						action="index.php?pid=<?php echo base64_encode("presentacion/adm/registroUsuario.php");?>"
						method="post">
	
						<?php if($error == 1){?>
							<div class="alert alert-danger" role="alert">Correo ya
							registrado, ingrese otro.</div>
						<?php }?>

						<input type="text" name="nombre" required id="textInput" class="form-control mb-4"
							placeholder="Nombre" value="<?php echo $nombre;?>"> 
							
						<input type="text" name="apellido" required id="textInput" class="form-control mb-4"
							placeholder="Apellido" value="<?php echo $apellido;?>">

						<input type="text" name="telefono" required id="textInput" class="form-control mb-4"
							placeholder="Telefono" value="<?php echo $telefono;?>"> 

						<input type="email" name="correo" required id="emailInput" class="form-control mb-4"
							placeholder="Correo"> 
						<input type="text" id="passwdInput" name="clave" required
							class="form-control mb-4" placeholder="Clave"> <small
							id="defaultRegisterFormPhoneHelpBlock"
							class="form-text text-muted mb-4">Debe tener mayusculas,
							minusculas.
						</small> 
						<button type="input" class="btn btn-primary" name="registro">Registrar
							usuario</button>
						<a
							href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php")?>"><button
							type="button" class="btn btn-secondary">Volver</button></a>


					</form>
				</div>
			</div>
		</div>

	</div>
</div>