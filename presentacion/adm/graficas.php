<?php
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();
include 'presentacion/menuAdministrador.php';
$cotizacion = new cotizacion();
$solicitud = new solicitud();
$material = new Material();
$cotizacionesporObrero = $cotizacion->consultarAgrupadoporObrero();
$tipodesolicitudes= $solicitud->consultaragrupadoportiposolicitud();
$cantidadMaterial=$material->vercantidadMaterial();
?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-primary text-white">GRAFICAS</div>
				<div class="card-body">
          SOLICITUDES SOLUCIONADAS POR OBREROS
					<canvas id="solicitudesporObrero" width="400" height="100"></canvas>
          TIPOS DE SOLICITUDES REALIZADAS POR CLIENTES
          <canvas id="solicitudesp" width="400" height="100"></canvas>
          VER CANTIDAD DE MATERIAL EN BODEGA
          <canvas id="materialesBodega" width="400" height="150"></canvas>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
  var ctx=document.getElementById("solicitudesporObrero").getContext("2d");
  var ctx2=document.getElementById("solicitudesp").getContext("2d");
  var ctx3=document.getElementById("materialesBodega").getContext("2d")
  var myChart= new Chart(ctx,{
            type:"pie",
            data:{
              <?php
                $labels="labels:[";
                $data="data:[";
                $rgb="";
                foreach ($cotizacionesporObrero as $p ) {
                  if ($p === end($cotizacionesporObrero)) {
                    $labels.="'".$p[1]."'";
                    $data.=$p[0];
                    $rgb.="'rgb(".rand (25,250).",".rand (25,250).",".rand (25,250).")'";
                  }else{
                    $labels.="'".$p[1]."',";
                    $data.=$p[0].",";
                    $rgb.="'rgb(".rand (25,250).",".rand (25,250).",".rand (25,250).")',";
                  }

                }
                $labels.="]";
                $data.="]";

               ?>
                <?php
                  echo $labels;
                ?>,
                datasets:[{
                        label:'Num datos',
                        <?php echo $data;?>,
                        backgroundColor:[
                          <?php echo $rgb;?>
                        ]
                }]
            }
        });
        var myChart2= new Chart(ctx2,{
                  type:"doughnut",
                  data:{
                    <?php
                      $labels="labels:[";
                      $data="data:[";
                      $rgb="";
                      foreach ($tipodesolicitudes as $p ) {
                        if ($p === end($tipodesolicitudes)) {
                          $labels.="'".$p[1]."'";
                          $data.=$p[0];
                          $rgb.="'rgb(".rand (25,250).",".rand (25,250).",".rand (25,250).")'";
                        }else{
                          $labels.="'".$p[1]."',";
                          $data.=$p[0].",";
                          $rgb.="'rgb(".rand (25,250).",".rand (25,250).",".rand (25,250).")',";
                        }

                      }
                      $labels.="]";
                      $data.="]";

                     ?>
                      <?php
                        echo $labels;
                      ?>,
                      datasets:[{
                              label:'Num datos',
                              <?php echo $data;?>,
                              backgroundColor:[
                                <?php echo $rgb;?>
                              ]
                      }]
                  }
              });
              var myChart3= new Chart(ctx3,{
                        type:"bar",
                        data:{
                          <?php
                            $labels="labels:[";
                            $data="data:[";
                            $rgb="";
                            foreach ($cantidadMaterial as $p ) {
                              if ($p === end($cantidadMaterial)) {
                                $labels.="'".$p[1]."'";
                                $data.=$p[0];
                                $rgb.="'rgb(".rand (25,250).",".rand (25,250).",".rand (25,250).")'";
                              }else{
                                $labels.="'".$p[1]."',";
                                $data.=$p[0].",";
                                $rgb.="'rgb(".rand (25,250).",".rand (25,250).",".rand (25,250).")',";
                              }

                            }
                            $labels.="]";
                            $data.="]";

                           ?>
                            <?php
                              echo $labels;
                            ?>,
                            datasets:[{
                                    label:'CANTIDAD',
                                    <?php echo $data;?>,
                                    backgroundColor:[
                                      <?php echo $rgb;?>
                                    ]
                            }]
                        }
                    });
</script>
