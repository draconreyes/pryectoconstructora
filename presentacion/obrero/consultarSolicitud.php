<?php
$obrero = new Obrero($_SESSION['id']);
$obrero->consultar();
$b= $obrero->disponible();
include 'presentacion/obrero/menuObrero.php';
?>

<div class="container">
	<div class="row">
		<div class="col-12">
		<div class="col-4">
			<div class="active-cyan-3 active-cyan-4 mb-4">
			<input class="form-control" type="text" id="filtro" onkeyup="actualizarBusqueda()" placeholder="Buscar Solicitud">
			</div>
		</div>
			<div class="card">
				<div class="card-header bg-primary text-white">Consultar Solicitudes</div>
				<div class="card-body">
					<div id="tabla">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalPaciente" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>

<script type="text/javascript">
function actualizarBusqueda() {
	<?php echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/obrero/tablaSolicitud.php") ."&id=".$_SESSION['id']."\";"; ?>
	$("#tabla").load(ruta,{valor:document.getElementById('filtro').value});

}
</script>
