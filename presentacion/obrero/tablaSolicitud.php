<?php
$solicitud = new Solicitud();
$sol = $solicitud->consultarporNombreO($_REQUEST['valor']);
$cotizacion = new cotizacion();
$obrero = new Obrero($_SESSION['id']);
?>

<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th scope="col">Id</th>
			<th scope="col">Descripcion</th>
			<th scope="col">direccion</th>
			<th scope="col">tipo</th>
			<th scope="col">Id_user</th>
			<th scope="col">foto</th>
			<th scope="col">Servicio</th>
		</tr>
	</thead>
	<tbody>
<?php
foreach ($sol as $p) {
        // Esta capa correspondiente a la fila del paciente a actualizar permitira agregar subcapas de estado y el candado a cambiar
        echo "<tr id='pac" . $p->getIdsolicitud() . "'>";

        echo "<td>" . $p->getIdsolicitud() . "</td>";

        echo "<td>" . $p->getDescripcion() . "</td>";

        echo "<td>" . $p->getDireccion() . "</td>";

        echo "<td>" . $p->getTipo() . "</td>";

        echo "<td>" . $p->getUsuario_idusuario() . "</td>";

        echo "<td>" . "<a href='index.php?pid=".base64_encode("presentacion/usuario/modalSolicitud.php")."&foto=".$p->getFoto ()."' data-toggle='modal' data-target='#modalSolicitud' >
						           <span class='far fa-image' data-toggle='tooltip' class='tooltipLink' data-placement='left'
						           data-original-title='Ver foto' ></span> </a> </td>";

        echo "<td>".((!($cotizacion->ObreroOcupado($obrero->getId())))?"<a href='index.php?pid=".base64_encode("presentacion/obrero/cotizar.php")."&idsolicitud=".$p->getIdsolicitud()."'>
						           <span class='fas fa-address-book' data-toggle='tooltip' class='tooltipLink' data-placement='left' data-original-title='cotizar' ></span> </a>":"<span class='fas fa-user-slash' data-toggle='tooltip' class='tooltipLink' data-placement='left'
						           data-original-title='Usted tiene trabajo pendiente'></span>")."</td>";
        echo "</tr>";
    }
    echo "<tr><td colspan='9'>" . count($sol) . " registros encontrados</td></tr>"?>
						</tbody>
</table>
<div class="modal fade" id="modalSolicitud" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
		<div class="modal-content" id="modalContent"></div>
</div>
</div>
