<?php
include 'presentacion/obrero/menuObrero.php';
$obrero = new Obrero($_SESSION['id']);
$obrero->consultar();
$reg=0;
if (isset($_GET['registrar'])) {
  $reg=1;
}
?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<?php if($reg == 1){
        ?>
					<div class="alert alert-success" role="alert">
					Cotrizacion exitosa ---
					</div>
				<?php }?>
				<div class="card-header bg-primary text-white">Bienvenido Usuario</div>
				<div class="card-body">
					<p>Usuario: <?php echo $obrero -> getNombre() . " " . $obrero -> getApellido() ?></p>
					<p>Correo: <?php echo $obrero -> getCorreo(); ?></p>
					<p>Hoy es: <?php echo date("d-M-Y"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
