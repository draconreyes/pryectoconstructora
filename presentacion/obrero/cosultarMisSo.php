<?php
$obrero = new Obrero($_SESSION['id']);
$obrero->consultar();
$cotizacion = new cotizacion();
$sol = $cotizacion->buscarCotizacionObrero($_SESSION['id']);
include 'presentacion/obrero/menuObrero.php';
?>
<div class="container">
	<div class="row">
		<div class="col-12">
		<div class="col-4">
		</div>
			<div class="card">
				<div class="card-header bg-primary text-white">Consultar Mis Solicitudes</div>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col">Direccion</th>
								<th scope="col">Descripcion</th>
								<th scope="col">Nombre cliente</th>
								<th scope="col">Foto</th>
								<th scope="col">Cotizacion</th>
								<th scope="col">Servicios</th>
							</tr>
						</thead>
						<tbody>
					<?php
					foreach ($sol as $p) {
					        // Esta capa correspondiente a la fila del paciente a actualizar permitira agregar subcapas de estado y el candado a cambiar
					        echo "<tr id='tut" . $p[5] . "'>";

									echo "<td>" . $p[0] . "</td>";

					        echo "<td>" . $p[1] . "</td>";

					        echo "<td>" . $p[2] . "</td>";

									echo "<td>" . "<a href='index.php?pid=".base64_encode("presentacion/usuario/modalSolicitud.php")."&foto=".$p[3]."' data-toggle='modal' data-target='#modalSolicitud' >
											           <span class='far fa-image' data-toggle='tooltip' class='tooltipLink' data-placement='left'
											           data-original-title='Ver foto' ></span> </a> </td>";

					        echo "<td> <a href='pdf/cotizaciones/".$p[4]."'  download='pdf/cotizaciones/".$p[4]."'>
									<span class='fas fa-file-pdf' data-toggle='tooltip' class='tooltipLink' data-placement='left'
									data-original-title='Descargar cotizacion' ></span>
														</a></td>";

									echo"<td>";
									$estado=$cotizacion->estadoSolicitud($p[5]);
									if($estado==2){
												echo"<span id='apJur".$p[5]."'> <a id='cambiarEstado". $p[5] . "' class='fas fa-user-check' href='#apJur".$p[5]."' data-toggle='tooltip' data-placement='bottom' title='Marcar Solicitud como terminada por Obrero'> </a></span>";
									}else if($estado==3){
												echo"<span class='far fa-building' data-toggle='tooltip' class='tooltipLink' data-placement='left'
													data-original-title='Solicitud terminada por Obrero' ></span>";
									}else if($estado==4){
												echo"<span class='fas fa-check-double' data-toggle='tooltip' class='tooltipLink' data-placement='left'
													data-original-title='Solicitud terminada y aprobada por cliente' ></span>";
									}
									echo"</td>";
					        echo "</tr>";
					    }
					    echo "<tr><td colspan='9'>" . count($sol) . " registros encontrados</td></tr>"?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	<?php foreach ($sol as $p) { ?>
	$("#apJur<?php echo $p[5]; ?>").click(function(){
        <?php echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/obrero/editarEstadoSolicitud.php") . "&idSolicitud=" . $p[5] ."&id=".$_SESSION['id']. "\";\n"; ?>
        $("#tut<?php echo $p[5]; ?>").load(ruta);
        $("#cambiarEstado<?php echo $p[5]; ?>").tooltip('hide');
	});
	<?php } ?>
});
</script>


<div class="modal fade" id="modalSolicitud" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
