<?php
$ob = new Obrero($_SESSION['id']);
$ob->consultar();
include 'presentacion/obrero/menuObrero.php';
?>
<div class="container">
	<div class="row">
		<div class="col-12">
		<div class="col-4">
			<div class="active-cyan-3 active-cyan-4 mb-4">
			<input class="form-control" type="text" id="filtro" onkeyup="actualizarBusqueda()" placeholder="Buscar material">
			</div>
		</div>
			<div class="card">
				<div class="card-header bg-primary text-white">Consultar Material</div>
				<div class="card-body">
					<div id="tabla">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalPaciente" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>

<script type="text/javascript">
function actualizarBusqueda() {
	<?php echo "var ruta = \"index.php?pid=" . base64_encode("presentacion/obrero/tablaBusqueda.php") . "\";"; ?>
	$("#tabla").load(ruta,{valor:document.getElementById('filtro').value});
}
</script>
