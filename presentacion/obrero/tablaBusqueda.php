<?php
$material = new Material();
$mat = $material->consultarporNombre($_REQUEST['valor']);
?>

<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th scope="col">Id</th>
			<th scope="col">Nombre</th>
			<th scope="col">cantidad</th>
			<th scope="col">precio</th>
			<th scope="col">Servicio</th>
		</tr>
	</thead>
	<tbody>
<?php
foreach ($mat as $p) {
        // Esta capa correspondiente a la fila del paciente a actualizar permitira agregar subcapas de estado y el candado a cambiar
        echo "<tr id='pac" . $p->getId() . "'>";

        echo "<td>" . $p->getId() . "</td>";

        echo "<td>" . $p->getNombre() . "</td>";

        echo "<td>" . $p->getCantidad() . "</td>";

        echo "<td>" . $p->getPrecio() . "</td>";

        echo "<td>" ."<a class='fas fa-people-carry' href='index.php?pid=" . base64_encode("presentacion/jefe/pedirM.php") . "&idm=" . $p->getId() . "' data-toggle='tooltip' data-placement='left' title='Pedir material'> </a>". "</td>";

        echo "</tr>";
    }
    echo "<tr><td colspan='9'>" . count($mat) . " registros encontrados</td></tr>"?>
						</tbody>
</table>
