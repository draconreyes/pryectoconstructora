<?php
$solicitud = new Solicitud("","","","",$_SESSION['id'],"");
$sol = $solicitud->consultarporNombre($_REQUEST['valor']);
$cotizacion= new cotizacion();
?>

<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th scope="col">Id</th>
			<th scope="col">Descripcion</th>
			<th scope="col">direccion</th>
			<th scope="col">tipo</th>
			<th scope="col">foto</th>
			<th scope="col">Estado</th>
			<th scope="col">Servicio</th>
		</tr>
	</thead>
	<tbody>
<?php
foreach ($sol as $p) {
        // Esta capa correspondiente a la fila del paciente a actualizar permitira agregar subcapas de estado y el candado a cambiar
        echo "<tr id='tut" . $p->getIdsolicitud() . "'>";

        echo "<td>" . $p->getIdsolicitud() . "</td>";

        echo "<td>" . $p->getDescripcion() . "</td>";

        echo "<td>" . $p->getDireccion() . "</td>";

        echo "<td>" . $p->getTipo() . "</td>";

				echo "<td>" . "<a href='index.php?pid=".base64_encode("presentacion/usuario/modalSolicitud.php")."&foto=".$p->getFoto ()."' data-toggle='modal' data-target='#modalSolicitud' >
						           <span class='far fa-image' data-toggle='tooltip' class='tooltipLink' data-placement='left'
						           data-original-title='Ver foto' ></span> </a> </td>";
											 if($cotizacion->estadoSolicitud($p->getIdsolicitud())==0){
							 				  echo"<td> Cotizacion  no realizada </td>";
							 				}else if($cotizacion->estadoSolicitud($p->getIdsolicitud())==1 or $cotizacion->estadoSolicitud($p->getIdsolicitud())==2 ){
							 				  echo"<td> Cotizacion realizada </td>";
							 				}else if($cotizacion->estadoSolicitud($p->getIdsolicitud())==3){
							 				  echo"<td> Confirmacion de solicitud terminada por obrero </td>";
							 				}else if ($cotizacion->estadoSolicitud($p->getIdsolicitud())==4){
							 				    echo"<td> Confirmacion de solicitud aprobada por usuario </td>";
							 				}
				echo "<td>";
				if($cotizacion->estadoSolicitud($p->getIdsolicitud())>=1){
					echo"<a href='pdf/cotizaciones/".$cotizacion->pdfSolicitud($p->getIdsolicitud())."'  download='pdf/cotizaciones/".$cotizacion->pdfSolicitud($p->getIdsolicitud())."'> <span class='fas fa-file-pdf' data-toggle='tooltip' class='tooltipLink' data-placement='left'
					data-original-title='Descargar cotizacion' ></span> </a>";
				}
				if($cotizacion->estadoSolicitud($p->getIdsolicitud())==3){
							echo"<span id='apJur".$p->getIdsolicitud()."'> <a id='cambiarEstado". $p->getIdsolicitud() . "' class='fas fa-user-check' href='#apJur".$p->getIdsolicitud()."' data-toggle='tooltip' data-placement='bottom' title='Marcar Solicitud como aprobada por cliente'> </a></span>";
				}else if($cotizacion->estadoSolicitud($p->getIdsolicitud())==4){
					echo"<span class='fas fa-check-double' data-toggle='tooltip' class='tooltipLink' data-placement='left'
					data-original-title='Solicitud terminada y aprobada por cliente' ></span>";
				}


				echo"</td>";

        echo "</tr>";
    }
    echo "<tr><td colspan='9'>" . count($sol) . " registros encontrados</td></tr>"?>
						</tbody>
</table>

</table>
<div class="modal fade" id="modalSolicitud" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
		<div class="modal-content" id="modalContent"></div>
</div>
</div>


<script type="text/javascript">
$(document).ready(function(){
	<?php foreach ($sol as $p) { ?>
	$("#apJur<?php echo $p->getIdsolicitud(); ?>").click(function(){
        <?php echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/usuario/editarEstadoSolicitud.php") . "&idSolicitud=" . $p->getIdsolicitud() ."&id=".$_SESSION['id']. "\";\n"; ?>
        $("#tut<?php echo $p->getIdsolicitud(); ?>").load(ruta);
        $("#cambiarEstado<?php echo $p->getIdsolicitud(); ?>").tooltip('hide');
	});
	<?php } ?>
});
</script>
