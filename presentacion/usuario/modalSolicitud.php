<?php
$foto=$_GET['foto'];

?>
<div class="modal-header">
    <h5 class="modal-title">Foto Solicitud</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table table-striped table-hover">
        <tbody>
            <tr>
                <th width="20%">Foto</th>
                <td><?php echo "<img src='img/".$foto."'" ?></td>
            </tr>
        </tbody>
    </table>
</div>
