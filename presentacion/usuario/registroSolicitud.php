<?php
$error=0;
$descripcion = "";
$direccion = "";
$tipo="";
$usuario = new Usuario( $_SESSION ['id'] );
if (isset ($_POST ['registro'])) {
$descripcion = $_POST ['descripcion'];
$direccion = $_POST ['direccion'];
$tipo = $_POST ['tipo'];
echo "No entro por foto";
if (isset($_FILES['foto'])) {
    echo"entro por foto";
    $errors = array();
    $extensions = array("jpeg", "jpg", "png");
    $ext_archivo = explode(".", $_FILES['foto']['name']);
    $ext_archivo = end($ext_archivo);
    if (in_array($ext_archivo, $extensions) === false) {
        echo "error1";
        $errors[] = "Extension no permitada, escoja un archivo con extension JPEG, PNG o JPG.";
    }
    if ($_FILES['foto']['size'] > 2097152) {
        echo "error2";
        $errors[] = "El archivo no debe pesar mas de 2MB";
    }
    if (empty($errors)) {
        echo "llego";
        $hora = round(microtime(true) * 1000);
        $nombreFoto = $hora . "." . $ext_archivo;
        move_uploaded_file($_FILES['foto']['tmp_name'], "img/" . $nombreFoto);
        $solicitud= new Solicitud("",$descripcion,$direccion,$tipo,$usuario->getId(),$nombreFoto);
        $solicitud->registrar();
        header("Location: index.php?pid=". base64_encode("presentacion/usuario/sesionUsuario.php")."&solic=true&id= ". $solicitud->getIdsolicitud() );
    }
}
}
?>
<div class="container">
	<div class="row">
<?php
include 'presentacion/usuario/menuUsuario.php';
?>
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card" style="margin-top: 100px;">
				<div class="card-header bg-primary text-white text-center py-4">Registro Solicitud Problema Locativo</div>
				<div class="card-body">
					<form class="border border-light p-5"
						action="index.php?pid=<?php echo base64_encode("presentacion/usuario/registroSolicitud.php");?>"
						method="post" enctype="multipart/form-data">
						<?php
						if (!empty($errors)) {?>
						    <div class="alert alert-danger" role="alert"><?php echo $errors[0] ?></div>
					  <?php }?>
						<textarea class="form-control" id="textInput" rows="3" name="descripcion" required placeholder="Descripcion" value="<?php echo $descripcion;?>" ></textarea>
						<br>
							
						<input type="text" name="direccion" required id="textInput" class="form-control mb-4"
							placeholder="Direccion" value="<?php echo $direccion;?>">
						

						<div class="form-group" required="required">
    					<label name="tipo">Tipo de problema</label>
   						<select class="form-control" name="tipo">
      					<option value="Fontaneria">Fontaneria</option>
      					<option value="Electrico">Electrico</option>
      					<option value="Construccion">Construccion</option>
      					<option value="Carpinteria">Carpinteria</option>
      					<option value="Pintura">Pintura</option>
    					</select>
  						</div>
						<br>
						<label name="tipo">Foto de problema</label>
						<div class="form-group">
							<input type="file" name="foto" class="form-control" placeholder="Foto" required="required">
						</div>
						<button type="input" class="btn btn-primary" name="registro">Registrar Solicitud</button>
						<a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/sesionUsuario.php")?>"><button type="button" class="btn btn-secondary">Volver</button></a>


					</form>
				</div>
			</div>
		</div>

	</div>
</div>