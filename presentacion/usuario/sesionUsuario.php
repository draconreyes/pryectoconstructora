<?php
$reg = 0;
$usuario = new Usuario($_SESSION['id']);
$usuario->consultar();
include 'presentacion/usuario/menuUsuario.php';
if (isset($_GET['solic'])) {
    $reg = 1;
}
?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
			<?php if($reg == 1){?>
				<div class="alert alert-success" role="alert">
				 Solicitud registrada con exito
				</div>
			<?php }?>
				<div class="card-header bg-primary text-white">Bienvenido Usuario</div>
				<div class="card-body">
					<p>Usuario: <?php echo $usuario -> getNombre() . " " . $usuario -> getApellido() ?></p>
					<p>Correo: <?php echo $usuario -> getCorreo(); ?></p>
					<p>Hoy es: <?php echo date("d-M-Y"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
