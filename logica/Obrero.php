<?php

class Obrero extends Persona {
    private $obreroDAO;
    private $conexion;
    private $foto;
    private $telefono;
    
    
    
    /**
     * @return ObreroDAO
     */
    public function getObreroDAO()
    {
        return $this->obreroDAO;
    }

    /**
     * @return Conexion
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @return  <string, mixed>
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return  <string, mixed>
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param ObreroDAO $obreroDAO
     */
    public function setObreroDAO($obreroDAO)
    {
        $this->obreroDAO = $obreroDAO;
    }

    /**
     * @param Conexion $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    /**
     * @param Ambigous <string, mixed> $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
        $this->getObreroDAO()->setFoto($foto);
    }

    /**
     * @param Ambigous <string, mixed> $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    function Obrero ($id="", $nombre="", $apellido="", $correo="", $clave="",$telefono="",$foto=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);
        $this->conexion = new Conexion();
        $this->telefono=$telefono;
        $this->foto=$foto;
        $this->obreroDAO = new ObreroDAO($id, $nombre, $apellido, $correo, $clave,$telefono,$foto);
    }
    
    function autenticar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->obreroDAO->autenticar());
        if($this->conexion->numFilas()==1){
            $resultado = $this->conexion->extraer();
            $this->id = $resultado[0];
            $this->conexion->cerrar();
            return true;
        }
        else {
            $this->conexion->cerrar();
            return false;
        }
    }
    
    function consultar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->obreroDAO->consultar());
        $resultado = $this->conexion->extraer();
        $this->id=$resultado[0];
        $this->nombre=$resultado[1];
        $this->apellido=$resultado[2];
        $this->correo=$resultado[3];
        $this->telefono=$resultado[4];
        $this->foto=$resultado[5];
        $this->conexion->cerrar();
    }
    
    function existeCorreo() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->obreroDAO->existeCorreo());
        if($this->conexion->numFilas()==1){
            $this->conexion->cerrar();
            return true;
        }
        else{
            $this->conexion->cerrar();
            return false;
        }
        
    }
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> obreroDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    function actualizarFoto()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->obreroDAO->actualizarFoto());
        $this->conexion->cerrar();
    }
    function fotoExiste()
    {
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->obreroDAO->fotoExiste());
        if ($this->conexion->extraer()[0] === "" || !($this->conexion->extraer()[0])) {
            $this->conexion->cerrar();
            return 0;
        } else {
            $this->conexion->ejecutar($this->obreroDAO->fotoExiste());
            $resultado = $this->conexion->extraer()[0];
            $this->conexion->cerrar();
            return $resultado;
        }
    }
    
    function disponible(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> obreroDAO -> disponible());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return true;
        } else {
            $this -> conexion -> cerrar();
            return false;
        }
    }
    
}
?>