<?php


class JfVenta extends Persona{
    private $telefono;
    private $foto;
    private $conexion;
    private $jfventaDAO;
    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return mixed
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @return mixed
     */
    public function getJfventaDAO()
    {
        return $this->jfventaDAO;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * @param mixed $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    /**
     * @param mixed $jfventaDAO
     */
    public function setJfventaDAO($jfventaDAO)
    {
        $this->jfventaDAO = $jfventaDAO;
    }

    function JfVenta($id="", $nombre="", $apellido="", $correo="", $clave="",$telefono="",$foto=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);
        $this->conexion = new Conexion();
        $this->telefono=$telefono;
        $this->foto=$foto;
        $this->jfventaDAO = new JfVentaDAO($id, $nombre, $apellido, $correo, $clave,$telefono,$foto);
    }
    
    function autenticar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->jfventaDAO->autenticar());
        if($this->conexion->numFilas()==1){
            $resultado = $this->conexion->extraer();
            $this->id = $resultado[0];
            $this->conexion->cerrar();
            return true;
        }
        else {
            $this->conexion->cerrar();
            return false;
        }
        
    }
    
    function consultar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->jfventaDAO->consultar());
        $resultado = $this->conexion->extraer();
        $this->id=$resultado[0];
        $this->nombre=$resultado[1];
        $this->apellido=$resultado[2];
        $this->correo=$resultado[3];
        $this->telefono=$resultado[4];
        $this->foto=$resultado[5];
        $this->conexion->cerrar();
    }
    
    
    function consultarPedidos($valor){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->jfventaDAO->consultarPedidos($valor));
        $resultados = array();
        $i = 0;
        while (($registro = $this->conexion->extraer()) != null) {
            $resultados[$i] = new JfVenta($registro[0], $registro[2], $registro[3], $registro[1]);
            $i++;
        }
        $this->conexion->cerrar();
        return $resultados;
        
    }
    
    function enviar($nombre,$cantidad) {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->jfventaDAO->enviar($nombre,$cantidad));
        $this->conexion->cerrar();
    }
    
    function cambiar($idc) {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->jfventaDAO->cambiar($idc));
        $this->conexion->cerrar();
    }
}

?>