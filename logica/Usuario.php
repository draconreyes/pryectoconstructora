<?php

class Usuario extends Persona {
    private $usuarioDAO;
    private $conexion;
    private $telefono;
    
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param Ambigous <string, mixed> $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return UsuarioDAO
     */
    public function getUsuarioDAO()
    {
        return $this->usuarioDAO;
    }

    /**
     * @return Conexion
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    /**
     * @param UsuarioDAO $usuarioDAO
     */
    public function setUsuarioDAO($usuarioDAO)
    {
        $this->usuarioDAO = $usuarioDAO;
    }

    /**
     * @param Conexion $conexion
     */
    public function setConexion($conexion)
    {
        $this->conexion = $conexion;
    }

    function Usuario($id="", $nombre="", $apellido="", $correo="", $clave="",$telefono=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);
        $this->conexion = new Conexion();
        $this->telefono=$telefono;
        $this->usuarioDAO = new UsuarioDAO($id, $nombre, $apellido, $correo, $clave,$telefono);
    }
    
    function autenticar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->usuarioDAO->autenticar());
        if($this->conexion->numFilas()==1){
            $resultado = $this->conexion->extraer();
            $this->id = $resultado[0];
            $this->conexion->cerrar();
            return true;
        }
        else {
            $this->conexion->cerrar();
            return false;
        }
    }
    
    function consultar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->usuarioDAO->consultar());
        $resultado = $this->conexion->extraer();
        $this->id=$resultado[0];
        $this->nombre=$resultado[1];
        $this->apellido=$resultado[2];
        $this->correo=$resultado[3];
        $this->telefono=$resultado[4];
        $this->conexion->cerrar();
    }
    function existeCorreo() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->usuarioDAO->existeCorreo());
        if($this->conexion->numFilas()==1){
            $this->conexion->cerrar();
            return true;
        }
        else{
            $this->conexion->cerrar();
            return false;
        }
        
    }
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    function consultarporNombre($valor){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->usuarioDAO->consultarporNombre($valor));
        $resultados = array();
        $i = 0;
        while (($registro = $this->conexion->extraer()) != null) {
            $resultados[$i] = new usuario($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5]);
            $i++;
        }
        $this->conexion->cerrar();
        return $resultados;
        
    }
    
   
}
?>