<?php
class cotizacion{
  private $idcotizacion;
  private $pdf;
  private $estado;
  private $obrero_idobrero;
  private $solicitud_idsolicitud;
  private $conexion;
  private $cotizacionDAO;

  function cotizacion($idcotizacion="",$pdf="",$estado="",$obrero_idobrero="",$solicitud_idsolicitud=""){
      $this->idcotizacion=$idcotizacion;
      $this->pdf=$pdf;
      $this->estado=$estado;
      $this->obrero_idobrero=$obrero_idobrero;
      $this->solicitud_idsolicitud=$solicitud_idsolicitud;
      $this->cotizacionDAO=new cotizacionDAO($idcotizacion,$pdf,$estado,$obrero_idobrero,$solicitud_idsolicitud);
      $this->conexion = new Conexion();
  }
  function registrar(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this ->cotizacionDAO  -> registrar());
      $this -> conexion -> cerrar();
  }
  function cambiarEstadoObrero($idsolicitud){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this ->cotizacionDAO-> cambiarEstadoObrero($idsolicitud));
      $this -> conexion -> cerrar();
  }
  function cambiarEstadoCliente($idsolicitud){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this ->cotizacionDAO-> cambiarEstadoCliente($idsolicitud));
      $this -> conexion -> cerrar();
  }
  function buscarIdcotizacion(){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this->cotizacionDAO ->buscarIdcotizacion());
      $resultados = array();
      $i = 0;
      $registro = $this->conexion->extraer();
      $this->conexion->cerrar();
      return $registro[$i];

  }
  function buscarCotizacionObrero($id){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this->cotizacionDAO->buscarCotizacionObrero($id));
      $resultados = array();
      $i = 0;
      while (($registro = $this->conexion->extraer()) != null) {
          $resultados[$i] = array($registro[0], $registro[1], $registro[2], $registro[3],$registro[4],$registro[5]);
          $i++;
      }
      $this->conexion->cerrar();
      return $resultados;
  }
  function ObreroOcupado($idobrero){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this->cotizacionDAO->ObreroOcupado($idobrero));
      $resultados = array();
      $i = 0;
      while (($registro = $this->conexion->extraer()) != null) {
          $resultados[$i] = $registro[0];
          $i++;
      }
      if(count($resultados)>0){
        return true;
      }else{
        return false;
      }

  }

  function estadoSolicitud($idsolicitud){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->cotizacionDAO->estadoSolicitud($idsolicitud));
        $resultados = array();
        $i = 0;
        $resultados[0][0]=0;
        while (($registro = $this->conexion->extraer()) != null) {
              $resultados[$i] = array($registro[0]);
              $i++;
            }
        $this->conexion->cerrar();
        return $resultados[0][0];
  }
  function pdfSolicitud($idsolicitud){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this->cotizacionDAO->pdfSolicitud($idsolicitud));
      $resultados = array();
      $resultado= array();
      $registro = $this->conexion->extraer();
      $resultado[0]=$registro[0];
      return $resultado[0];
  }

  function buscarCotizacionSolicitud($id){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this->cotizacionDAO->buscarCotizacionSolicitud($id));
      $registro = $this->conexion->extraer();
      $resultados= array($registro[0], $registro[1], $registro[2], $registro[3],$registro[4],$registro[5]);
      $this->conexion->cerrar();
      return $resultados;
  }

  function consultarAgrupadoporObrero(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> cotizacionDAO -> consultarAgrupadoporObrero());
      $resultados = array();
      $i=0;
      while(($registro = $this -> conexion -> extraer()) != null){
          $resultados[$i][0] = $registro[0];
          $resultados[$i][1] = $registro[1];
          $i++;
      }
      $this -> conexion -> cerrar();
      return $resultados;
  }

}

?>
