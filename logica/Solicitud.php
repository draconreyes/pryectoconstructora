<?php

/**
 * @author Diego pe�a
 *
 */
class Solicitud {
    private $idsolicitud;
    private $descripcion;
    private $direccion;
    private $tipo;
    private $usuario_idusuario;
    private $foto;
    private $SolicitudDAO;
    private $conexion;

    function Solicitud($idsolicitud="", $descripcion="",$direccion="",$tipo="",$usuario_idusuario="",$foto="") {
        $this->idsolicitud=$idsolicitud;
        $this->descripcion=$descripcion;
        $this->direccion=$direccion;
        $this->tipo=$tipo;
        $this->usuario_idusuario=$usuario_idusuario;
        $this->foto=$foto;
        $this->conexion=new Conexion();
        $this->SolicitudDAO= new SolicitudDAO($idsolicitud, $descripcion,$direccion,$tipo,$usuario_idusuario,$foto);
    }

    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> SolicitudDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    function consultar() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->SolicitudDAO->registrar());
        $resultado = $this->conexion->extraer();
        $this->id=$resultado[0];
        $this->descripcion=$resultado[1];
        $this->tipo=$resultado[2];
        $this->usuario_idusuario=$resultado[3];
        $this->foto=$resultado[4];
        $this->conexion->cerrar();
    }
    // las solicitudes de un usuario
    function consultarporNombre($valor){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->SolicitudDAO->consultarporNombre($valor));
        $resultados = array();
        $i = 0;
        while (($registro = $this->conexion->extraer()) != null) {
            $resultados[$i] = new Solicitud($registro[0], $registro[1], $registro[2], $registro[3],"", $registro[4]);
            $i++;
        }
        $this->conexion->cerrar();
        return $resultados;

    }
    // las solicitudes que ningun obrero a aceptado
    function consultarporNombreO($valor){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->SolicitudDAO->consultarporNombreO($valor));
        $resultados = array();
        $i = 0;
        while (($registro = $this->conexion->extraer()) != null) {
            $resultados[$i] = new Solicitud($registro[0], $registro[1], $registro[2], $registro[3],$registro[4], $registro[5]);
            $i++;
        }
        $this->conexion->cerrar();
        return $resultados;

    }
    // los encargos que acepto el obrero
    function consultarporNombreM($valor){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->SolicitudDAO->consultarporNombreM($valor));
        $resultados = array();
        $i = 0;
        while (($registro = $this->conexion->extraer()) != null) {
            $resultados[$i] = new Solicitud($registro[0], $registro[1], $registro[2], $registro[3],$registro[4], $registro[5]);
            $i++;
        }
        $this->conexion->cerrar();
        return $resultados;

    }
    function consultarporId(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->SolicitudDAO->consultarporId());
        $resultados [0]= array();
        $i = 0;
        while (($registro = $this->conexion->extraer()) != null) {
            $resultados[$i] = new Solicitud($registro[0], $registro[1], $registro[2], $registro[3],$registro[4], $registro[5]);
            $i++;
        }
        $this->conexion->cerrar();
        return $resultados[0];
    }


    function consultaragrupadoportiposolicitud(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> SolicitudDAO -> consultaragrupadoportiposolicitud());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
          $resultados[$i][0] = $registro[0];
          $resultados[$i][1] = $registro[1];
          $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
}


    /**
     * @param Ambigous <string, mixed> $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }
    /**
     * @return  <string, mixed>
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return mixed
     */
    public function getIdsolicitud()
    {
        return $this->idsolicitud;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @return mixed
     */
    public function getUsuario_idusuario()
    {
        return $this->usuario_idusuario;
    }

    /**
     * @param mixed $idsolicitud
     */
    public function setIdsolicitud($idsolicitud)
    {
        $this->idsolicitud = $idsolicitud;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @param mixed $usuario_idusuario
     */
    public function setUsuario_idusuario($usuario_idusuario)
    {
        $this->usuario_idusuario = $usuario_idusuario;
    }







}
?>
