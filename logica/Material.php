<?php


class Material{
       private $id;
       private $nombre;
       private $cantidad;
       private $precio;
       private $conxion;
       private $materialDAO;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @return mixed
     */
    public function getConxion()
    {
        return $this->conxion;
    }

    /**
     * @return mixed
     */
    public function getMaterialDAO()
    {
        return $this->materialDAO;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @param mixed $conxion
     */
    public function setConxion($conxion)
    {
        $this->conxion = $conxion;
    }

    /**
     * @param mixed $materialDAO
     */
    public function setMaterialDAO($materialDAO)
    {
        $this->materialDAO = $materialDAO;
    }


    function Material($id="", $nombre="", $cantidad="", $precio=""){
        $this->conexion = new Conexion();
        $this->id=$id;
        $this->nombre=$nombre;
        $this->cantidad=$cantidad;
        $this->precio=$precio;
        $this->materialDAO = new MaterialDAO($id, $nombre, $cantidad, $precio);
    }

    function consultarporNombre($valor){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->materialDAO->consultarporNombre($valor));
        $resultados = array();
        $i = 0;
        while (($registro = $this->conexion->extraer()) != null) {
            $resultados[$i] = new Material($registro[0], $registro[1], $registro[2], $registro[3]);
            $i++;
        }
        $this->conexion->cerrar();
        return $resultados;

    }
    function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->materialDAO->consultar());
        $resultados = array();
        $registro = $this->conexion->extraer();
        $this->id=$registro[0];
        $this->nombre=$registro[1];
        $this->cantidad=$registro[2];
        $this->precio=$registro[3];
        $this->conexion->cerrar();
    }

    function pedirMa(){
        $this->conexion->abrir();

        $this->conexion->ejecutar($this->materialDAO->pedirMa());

        $this->conexion->cerrar();

    }

    function existeM() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->materialDAO->existeM());
        if($this->conexion->numFilas()==1){
            $this->conexion->cerrar();
            return true;
        }
        else{
            $this->conexion->cerrar();
            return false;
        }

    }

    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> materialDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    function vercantidadMaterial(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> materialDAO -> vercantidadMaterial());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
          $resultados[$i][0] = $registro[0];
          $resultados[$i][1] = $registro[1];
          $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
}

}
