<?php
class pedido{
  private $idpedido;
  private $cantidad;
  private $cotizacion_idcotizacion;
  private $inventario_idinventario;
  private $pedidoDAO;
  private $conexion;

  
  
  /**
     * @return string
     */
    public function getIdpedido()
    {
        return $this->idpedido;
    }

/**
     * @return string
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

/**
     * @return string
     */
    public function getCotizacion_idcotizacion()
    {
        return $this->cotizacion_idcotizacion;
    }

/**
     * @return string
     */
    public function getInventario_idinventario()
    {
        return $this->inventario_idinventario;
    }

/**
     * @param string $idpedido
     */
    public function setIdpedido($idpedido)
    {
        $this->idpedido = $idpedido;
    }

/**
     * @param string $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

/**
     * @param string $cotizacion_idcotizacion
     */
    public function setCotizacion_idcotizacion($cotizacion_idcotizacion)
    {
        $this->cotizacion_idcotizacion = $cotizacion_idcotizacion;
    }

/**
     * @param string $inventario_idinventario
     */
    public function setInventario_idinventario($inventario_idinventario)
    {
        $this->inventario_idinventario = $inventario_idinventario;
    }

function pedido($idpedido="",$cantidad="",$cotizacion_idcotizacion="",$inventario_idinventario=""){
      $this->idpedido=$idpedido;
      $this->cantidad=$cantidad;
      $this->cotizacion_idcotizacion=$cotizacion_idcotizacion;
      $this->inventario_idinventario=$inventario_idinventario;
      $this->pedidoDAO=new pedidoDAO($idpedido,$cantidad,$cotizacion_idcotizacion,$inventario_idinventario);
      $this->conexion = new Conexion();
  }
  
  
  
  function registrar(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> pedidoDAO -> registrar());
      $this -> conexion -> cerrar();
  }
  function consultarporId($valor){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this-> pedidoDAO->consultarporId($valor));
      $resultados = array();
      $i = 0;
      while (($registro = $this->conexion->extraer()) != null) {
          $resultados[$i] = array($registro[0], $registro[1], $registro[2], $registro[3],$registro[4],$registro[5]);
          $i++;
      }
      $this->conexion->cerrar();
      return $resultados;

  }
  function valorApagarId($valor){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this-> pedidoDAO->valorApagarId($valor));
      $resultados = array();
      $registro = $this->conexion->extraer();
      $this->conexion->cerrar();
      return $registro[0];

  }

  function materiales(){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this-> pedidoDAO->materiales());
      $resultados = array();
      $i = 0;
      while (($registro = $this->conexion->extraer()) != null) {
          $resultados[$i] = new pedido($registro[0], $registro[1],$this->cotizacion_idcotizacion, $registro[2]);
          $i++;
      }
      $this->conexion->cerrar();
      return $resultados;
      
  }

}

?>
