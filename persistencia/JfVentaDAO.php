<?php

class JfVentaDAO extends Persona {

    private $foto;
    private $telefono;
    /**
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    function JfVentaDAO($id="", $nombre="", $apellido="", $correo="", $clave="",$telefono="",$foto=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);
        $this->foto=$foto;
        $this->telefono=$telefono;

    }

    function autenticar() {
        return "SELECT idj_bodega
                FROM j_bodega
                WHERE correo = '" . $this->correo . "' AND
                clave = md5('" . $this->clave . "');";

    }

    function consultar(){
        return "select idj_bodega, nombre, apellido, correo,telefono,foto from j_bodega
                where idj_bodega = '" . $this -> id . "'";
    }

    function consultarPedidos($valor)
    {
        return "select c.idcotizacion, s.tipo,b.nombre, b.apellido from obrero as b, solicitud as s, cotizacion as c
                where b.idobrero = c.obrero_idobrero and
                c.solicitud_idsolicitud = s.idsolicitud and c.estado = '1' and s.tipo like '%".$valor."%'";
    }

    function enviar($nombre,$cantidad)
    {
        return "update inventario set cantidad =cantidad-".$cantidad." where nombre = '".$nombre."'";
    }

    function cambiar($idc)
    {
        return "update cotizacion set estado ='2' where idcotizacion=".$idc;
    }
}

?>
