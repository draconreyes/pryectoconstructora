<?php
class cotizacionDAO{
  private $idcotizacion;
  private $pdf;
  private $estado;
  private $obrero_idobrero;
  private $solicitud_idsolicitud;

  function cotizacionDAO($idcotizacion="",$pdf="",$estado="",$obrero_idobrero="",$solicitud_idsolicitud=""){
      $this->idcotizacion=$idcotizacion;
      $this->pdf=$pdf;
      $this->estado=$estado;
      $this->obrero_idobrero=$obrero_idobrero;
      $this->solicitud_idsolicitud=$solicitud_idsolicitud;
  }
  function buscarIdcotizacion() {
    return "SELECT idcotizacion from cotizacion where obrero_idobrero=".$this->obrero_idobrero." AND solicitud_idsolicitud=".$this->solicitud_idsolicitud.";";
  }
  function registrar() {
      return "INSERT INTO cotizacion (pdf, estado, obrero_idobrero,solicitud_idsolicitud)
      VALUES ('".$this->pdf."',".$this->estado.",".$this->obrero_idobrero.",".$this->solicitud_idsolicitud.");";
  }
  function buscarCotizacionObrero($id) {
    return  "SELECT S.direccion,S.descripcion,U.nombre,S.foto,C.pdf,C.solicitud_idsolicitud  FROM solicitud S, cotizacion C, usuario U
            where C.solicitud_idsolicitud=S.idsolicitud AND  S.usuario_idusuario=U.idusuario  AND  C.obrero_idobrero=".$id;
      }
  function buscarCotizacionSolicitud($id) {
    return  "SELECT S.direccion,S.descripcion,U.nombre,S.foto,C.pdf,C.solicitud_idsolicitud  FROM solicitud S, cotizacion C, usuario U
                where C.solicitud_idsolicitud=S.idsolicitud AND  S.usuario_idusuario=U.idusuario  AND  C.solicitud_idsolicitud=".$id;
    }

  function ObreroOcupado($idobrero){
    return "SELECT C.idcotizacion FROM cotizacion C  WHERE   (C.estado='1'  OR C.estado='2'  OR C.estado='3') AND C.obrero_idobrero=".$idobrero;
  }
  function estadoSolicitud($idsolicitud){
    return  "SELECT C.estado FROM cotizacion C WHERE C.solicitud_idsolicitud=".$idsolicitud;
  }
  function pdfSolicitud($idsolicitud){
    return "SELECT C.pdf FROM cotizacion C WHERE C.solicitud_idsolicitud=".$idsolicitud;
  }

  function cambiarEstadoObrero($idsolicitud){
    return "UPDATE cotizacion SET estado=3 WHERE cotizacion.solicitud_idsolicitud=".$idsolicitud;
  }
  function cambiarEstadoCliente($idsolicitud){
    return "UPDATE cotizacion SET estado=4 WHERE cotizacion.solicitud_idsolicitud=".$idsolicitud;
  }
  function consultarAgrupadoporObrero(){
    return "SELECT count(C.idcotizacion), O.nombre,O.apellido FROM cotizacion C, obrero O WHERE  C.obrero_idobrero=O.idobrero AND  C.estado='4' GROUP BY O.idobrero";

  }

}

?>
