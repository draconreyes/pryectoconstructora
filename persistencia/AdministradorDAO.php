<?php
class AdministradorDAO extends Persona{
    private $foto;
    private $telefono;
    /**
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    function AdministradorDAO ($id="", $nombre="", $apellido="", $correo="", $clave="",$telefono="",$foto=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);
        $this->foto=$foto;
        $this->telefono=$telefono;
        
    }
    
    function autenticar() {
        return "SELECT idadministrador 
                FROM administrador
                WHERE correo = '" . $this->correo . "' AND
                clave = md5('" . $this->clave . "');";
                  
    }
    
    function consultar(){
        return "select idadministrador, nombre, apellido, correo,telefono,foto from administrador
                where idadministrador = '" . $this -> id . "'";
    }
    
    function consultarProyCurr() {
    	return "SELECT idproyecto_curricular, proyecto_curricular
				FROM proyecto_curricular";
    }
    
}

?>