<?php
class pedidoDAO{
  private $idpedido;
  private $cantidad;
  private $cotizacion_idcotizacion;
  private $inventario_idinventario;

  function pedidoDAO($idpedido="",$cantidad="",$cotizacion_idcotizacion="",$inventario_idinventario=""){
      $this->idpedido=$idpedido;
      $this->cantidad=$cantidad;
      $this->cotizacion_idcotizacion=$cotizacion_idcotizacion;
      $this->inventario_idinventario=$inventario_idinventario;
  }
  function registrar() {
      return  "INSERT INTO pedido (cantidad, cotizacion_idcotizacion, inventario_idinventario)
      VALUES (".$this->cantidad.",".$this->cotizacion_idcotizacion.",".$this->inventario_idinventario.");";
  }
  function consultarporId($valor)
  {
      return "SELECT P.idpedido, P.cantidad, P.cotizacion_idcotizacion, P.inventario_idinventario,I.precio,I.nombre
          FROM pedido AS P ,inventario as I where P.cotizacion_idcotizacion=".$valor." AND I.idinventario
          IN (SELECT P.inventario_idinventario FROM pedido P WHERE P.cotizacion_idcotizacion=". $valor .")
          AND P.inventario_idinventario=I.idinventario";
  }

  function valorApagarId($valor){
    return "SELECT SUM(P.cantidad * I.precio)
            FROM pedido AS P ,inventario as I where P.cotizacion_idcotizacion=".$valor." AND I.idinventario
            IN (SELECT P.inventario_idinventario FROM pedido P WHERE P.cotizacion_idcotizacion=".$valor.")
            AND P.inventario_idinventario=I.idinventario";

  }
  
  function materiales(){
      return "select i.nombre, i.cantidad as cant_inventario, p.cantidad from inventario as i, pedido as p where 
              p.inventario_idinventario = i.idinventario and p.cotizacion_idcotizacion = ".$this->cotizacion_idcotizacion;
  }


}




?>
