<?php
class MaterialDAO {
    private $id;
    private $nombre;
    private $cantidad;
    private $precio;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }


    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }




    function MaterialDAO($id="", $nombre="", $cantidad="", $precio=""){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->cantidad=$cantidad;
        $this->precio=$precio;
    }

    function consultarporNombre($valor)
    {
        return "SELECT * FROM inventario where nombre like '%".$valor."%'";
    }
    function consultar()
    {
        return  "SELECT * FROM inventario where idinventario=".$this->id;
    }


    function pedirMa(){
        return "update inventario set cantidad = cantidad +". $this->cantidad . " where idinventario =" .$this->id;
    }

    function existeM() {
        return "SELECT idinventario
					FROM inventario
					WHERE nombre = '" . $this->nombre ."';";
    }

    function registrar() {
        return  "INSERT INTO inventario (nombre, cantidad, precio)
        VALUES ('".$this->nombre."','".$this->cantidad."','".$this->precio."');";
    }
    function vercantidadMaterial(){
      return "SELECT I.cantidad,I.nombre FROM inventario I";
    }
}
