<?php
require('pdf/fpdf.php');
require('logica/pedido.php');
require('persistencia/pedidoDAO.php');
require('persistencia/Conexion.php');
date_default_timezone_set('America/Bogota');
class pdf extends FPDF{

  public function header()
  {
      $this->SetFont('Courier','B',12);
      $this->Cell(0,5,date("d/m/yy"),0,0,'C');
      $this->Image('img/logo.jpg',190,5,20,20);
  }
  public function Footer()
  {
    $this->SetFont('Courier','B',12);
    $this->SetY(-15);
    $this->write(5,'Obrero:'. $_GET['ido']);

  }
  public function Inicio(){
    $this->AddPage('portrait','letter');
    $this->SetFont('Arial','B',14);
    $this->ln(3);
    $this->Image('img/logo.jpg',83,20,50,50);
    $this->ln(55);
    $this->Cell(0,5,'Cotizacion N '.$_GET['ids'],0,0,'C');
    $this->ln(10);
  }
  public function Registros(){
    $this->SetFontSize(10);
    $this->SetFont('Arial','B');
    $this->SetFillColor(255,255,255);
    $this->SetTextColor(40,40,40);
    $this->SetDrawColor(88,88,88);
    $this->Cell(40,10,'Material',0,0,'C',1);
    $this->Cell(50,10,'Cantidad',0,0,'C',1);
    $this->Cell(50,10,'Valor',0,0,'C',1);
    $this->Cell(50,10,'Total',0,0,'C',1);
    $this->SetDrawColor(61,134,123);
    $this->SetLineWidth(1);
    $this->Line(15,87,195,87);
    $this->SetTextColor(0,0,0);
    $this->ln();
    $pedido= new pedido();
    $arreglo = array();
    $arreglo=$pedido->consultarporId($_GET['ids']);
    foreach ($arreglo as $p) {
      $this->SetFillColor(189, 203, 252 );
      $this->SetTextColor(40,40,40);
      $this->SetDrawColor(88,88,88);
      $this->Cell(40,10,$p[5],0,0,'C',1);
      $this->Cell(50,10,$p[1],0,0,'C',1);
      $this->Cell(50,10,"$".$p[4],0,0,'C',1);
      $this->Cell(50,10,"$".$p[1]*$p[4],0,0,'C',1);
      $this->ln();
  }
  $this->SetFillColor(254, 103, 66  );
  $this->Cell(172,10,"TOTAL APAGAR $".$pedido->valorApagarId($_GET['ids']),0,0,'R',1);
  $this->Output();
  $this->Output('F','pdf/cotizaciones/'.$_GET['name'].'.pdf');
}
}
$pdf= new pdf();
$pdf->Inicio();
$pdf->Registros();

?>
