<?php
session_start();
include_once 'logica/Persona.php';
include_once 'logica/Usuario.php';
include_once 'logica/Administrador.php';
include_once 'logica/Obrero.php';
include_once 'logica/JfVenta.php';
include_once 'logica/Material.php';
include_once 'logica/Solicitud.php';
include_once 'logica/pedido.php';
include_once 'logica/cotizacion.php';
include_once 'persistencia/cotizacionDAO.php';
include_once 'persistencia/pedidoDAO.php';
include_once 'persistencia/ObreroDAO.php';
include_once 'persistencia/AdministradorDAO.php';
include_once 'persistencia/Conexion.php';
include_once 'persistencia/ObreroDAO.php';
include_once 'persistencia/UsuarioDAO.php';
include_once 'persistencia/JfVentaDAO.php';
include_once 'persistencia/MaterialDAO.php';
include_once 'persistencia/SolicitudDAO.php';


?>

<!DOCTYPE html>
<html lang="es">
<head>
<link rel="stylesheet" 	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" 	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<link rel="stylesheet" 	href="css/normalize.css">
<link rel="stylesheet" href="css/flaty.min.css">
<link rel="stylesheet" href="css/estilo.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" 	href="css/styles.css">
<!-- https://tailwindcss.com/ Framework -->
<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
<link href="https://fonts.googleapis.com/css?family=Lexend+Deca|Lexend+Giga&display=swap" rel="stylesheet">

</head>

<body>

<?php
  /*  if (isset($_GET["pid"])) {
        $pid = base64_decode($_GET["pid"]);
        if ($_SESSION['id'] != "" || isset($_GET['nos'])) {
            include $pid;
        } else {
            header("Location: index.php");
        }
    } else {
        $_SESSION['id'] = "";
        include 'presentacion/inicio.php';
    }*/
    ?>

    	<?php
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
    if(isset($_GET["nos"]) || (!isset($_GET["nos"]) && $_SESSION['id']!="")){
        include $pid;
    }else{
        header("Location: index.php");
    }
} else {
    $_SESSION['id']="";
    include 'presentacion/inicio.php';
}

?>

<script type="text/javascript" src="js/scripts.js"></script>

</body>

</html>
